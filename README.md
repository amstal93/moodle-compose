# moodle-compose

A (loose) docker-compose setup for Moodle

## Usage

```bash
cd /srv/moodle-compose/
cp env.example .env && editor .env # set a default login/password
docker-compose up -d
```

Give it some time to perfom some initialization tasks (~10mn).

Then, point your browser at <http://${HOST}:8080/> and start learning (or teaching)!

## Multiply

The `supervixen` hypervisor at BSF HQ has a `tpl-moodle` template
that you can clone at will.

For now the setup has to be tweaked manually:

```bash
sudo hostnamectl set-hostname moodle-wtf
sudo sed -i -e 's/tpl-moodle/moodle-wtf' /etc/hosts
cd /etc/ssh/
sudo rm -f ssh_host_* && sudo dpkg-reconfigure openssh-server
sudo shutdown -r now # the lazy way to get the router to set RDNS
```

From now on, you should be able to connect to `moodle-wtf` via SSH,
and start the stack.
